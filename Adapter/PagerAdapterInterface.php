<?php

/*
 * This file is part of the PagerBundle package.
 *
 * (c) Marcin Butlak <contact@maker-labs.com>
 * (c) Zephyr Web <contact@zephyr-web.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Zephyr\PagerBundle\Adapter;

/**
 * Pager adapter interface
 * 
 * @author Marcin Butlak <contact@maker-labs.com>
 * @author Zephyr Web <contact@zephyr-web.eu>
 */
interface PagerAdapterInterface
{
    /**
     * Returns the list of results 
     * 
     * @return array 
     */
    function getResults($offset, $limit);

    /**
     * Returns the total number of results
     * 
     * @return integer
     */
    function getTotalResults();
}
