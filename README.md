README
======

Features
--------

 * Twig and PHP template support
 * Easy to customize and extend 
 * DoctrineORM and Array adapters support
 
## Installation
 * Copy the PagerBundle to /src/MakerLabs/PagerBundle
 * Register the bundle in your AppKernel
 * Run "./app/console assets:install web" command
 * Your done.

## Usage
### Controller

```php
<?php
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MakerLabs\PagerBundle\Pager;
use MakerLabs\PagerBundle\Adapter\ArrayAdapter;

class ExampleController extends Controller
{
   /**
    *
    * @Route("/example/{page}", defaults={"page"=1}, name="example_route")
    * @Template()
    */
   public function exampleAction($page)
   {
      $array = range(1, 100);
      $adapter = new ArrayAdapter($array);
      $pager = new Pager($adapter, array('page' => $page, 'limit' => 25));

      return array('pager' => $pager);
   }
}
```

### Twig template

```html
{% if pager.isPaginable %}
   {{ paginate(pager, 'example_route') }}
{% endif %}
{% for item in pager.getResults %}
   <p>{{ item }}</p>
{% endfor %}
```

### PHP template

```php
<?php if ($pager->isPaginable()): ?>
    <?php echo $view['pager']->paginate($pager, 'example_route') ?>
<?php endif; ?>
<?php foreach ($pager->getResults() as $item): ?>
    <p><?php echo $item ?></p>
<?php endforeach; ?>
```

### Default CSS styles
There are two default css styles located in _web/bundles/makerlabspager/css/_ (clean.css and round.css). You must include them manually in the layout template.
## Adapters
### ArrayAdapter

```php
<?php
use MakerLabs\PagerBundle\Adapter\ArrayAdapter;
/* ... */
$adapter = new ArrayAdapter($your_array);
```

### DoctrineOrmAdapter

```php
<?php
use MakerLabs\PagerBundle\Adapter\DoctrineOrmAdapter;
/* ... */
$em = $this->getDoctrine()->getEntityManager();               
$qb = $em->getRepository('ExampleEntity')->createQueryBuilder('f');
$adapter = new DoctrineOrmAdapter($qb);
```

## Customization
### Passing additional parameters to the pager links
You can pass as many additional parameters as you will fit. For example lets modify our route **example_route** and pass an additional **{type}** parameter:

```
@Route("/example/{page}/{type}", defaults={"page"=1}, name="example_route")
```

Twig template:

```
{{ paginate(pager, 'example_route', {'type': 'long'}) }}
```

PHP template:

```php
<?php echo $view['pager']->paginate($pager, 'example_route', array('type': 'long')) ?>
```

### Changing the default 'page' parameter name
For example lets modify our route **example_route** and change the default **{page}** parameter to **{offset}**: 

```
@Route("/example/{offset}", defaults={"offset"=1}, name="example_route")
```

Twig template:

```
{{ paginate(pager, 'example_route', {'_page': 'offset'}) }}
```

PHP template:

```php
<?php echo $view['pager']->paginate($pager, 'example_route', array('_page': 'offset')) ?>
```

### Modifying the default rendering template
There are 2 default rendering templates located in _PagerBundle/Resources/views/Pager_:

 * Twig: paginate.html.twig
 * PHP: paginage.html.php

If you want to customize their HTML simply copy one of them to _/app/Resources/MakerLabsPagerBundle/views/Pager_

Alternatively you can pass the name of your template (in bundle:section:template.format.engine format) to the paginate helper:

 * Twig template:

    ```
    {{ paginate(pager, 'example_route', {}, 'ExampleBundle:Example:name.html.twig') }}
    ```

 * PHP template:

    ```php
    <?php echo $view['pager']->paginate($pager, 'example_route', array(), 'ExampleBundle:Example:name.html.php') ?>
    ```

